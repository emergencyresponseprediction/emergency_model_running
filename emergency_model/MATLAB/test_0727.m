data = readtable("test1.csv");
x = data.CreationDate;
y = data.ResponseTime;

h = plot(x,y);
ax = ancestor(h, 'axes');
ax.YAxis.Exponent = 0;
ytickformat('%.0f');
box off

ylim([0,120000]);
set(gca,'TickDir','out')
set(h,'LineWidth',0.1)
xlabel("Creation Date")
ylabel("Response Time")
pbaspect([8 1 1])